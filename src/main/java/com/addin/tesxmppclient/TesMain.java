/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addin.tesxmppclient;

import java.io.Console;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatManager;
import org.jivesoftware.smack.chat.ChatMessageListener;
import org.jivesoftware.smack.java7.Java7HostnameVerifier;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.roster.Roster;
import org.jivesoftware.smack.roster.RosterEntry;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.filetransfer.FileTransfer;
import org.jivesoftware.smackx.filetransfer.FileTransferManager;
import org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer;
import org.jivesoftware.smackx.vcardtemp.VCardManager;
import org.jivesoftware.smackx.vcardtemp.packet.VCard;

/**
 *
 * @author addin
 */
public class TesMain {

    public static void main(String[] args) {
        try {

            SmackConfiguration.DEBUG = true;
            XMPPTCPConnectionConfiguration conf = XMPPTCPConnectionConfiguration.builder()
                    .setServiceName("chat.facebook.com")
                    .setConnectTimeout(90000)
                    .setSecurityMode(ConnectionConfiguration.SecurityMode.required)
                    .setDebuggerEnabled(true)
                    .setHostnameVerifier(new Java7HostnameVerifier())
                    .build();

            System.out.println("Timeout set to: " + conf.getConnectTimeout());

            AbstractXMPPConnection connection = new XMPPTCPConnection(conf);
            System.out.println("Connecting...");
            connection.connect();
            amIConnected(connection);
            if (connection.isSecureConnection()) {
                System.out.println("The connection is secure.");
            }

            System.out.println("Logging in...");
            connection.login("email", "password");
            if (connection.isAuthenticated()) {
                System.out.println("You logged in as " + connection.getUser());
                VCard v = VCardManager.getInstanceFor(connection).loadVCard();
                System.out.println("" + v.getField("FN"));
                
                Roster roster = Roster.getInstanceFor(connection);
                ChatManager chatManager = ChatManager.getInstanceFor(connection);
                for (RosterEntry entry : roster.getEntries()) {
                    if (entry.getName().toLowerCase().contains("ruchdi")) {
                        System.out.println("" + entry.getUser() + " | " + entry.getName());
                        Chat chat = chatManager.createChat(entry.getUser(), new ChatMessageListener() {

                            @Override
                            public void processMessage(Chat arg0, Message arg1) {
                                System.out.println(arg0.getParticipant()+" respond: "+arg1.getBody());
                            }
                        });
                        System.out.println("Sending message to "+entry.getName());
                        Message m = new Message();
                        m.setFrom(connection.getUser());
                        m.setTo(entry.getUser());
                        m.setBody("This message is test message. It was created programatically and sent to you to test if i was doing it right. :D");
                        chat.sendMessage(m);
                        System.out.println("connection.getUser() = " + connection.getUser());
                        
                        FileTransferManager ftm = FileTransferManager.getInstanceFor(connection);
                        OutgoingFileTransfer oft = ftm.createOutgoingFileTransfer(entry.getUser()+"/Smack");
                        oft.sendFile(new File("pom.xml"), "file transfer test.");
                    }
                }
            }

            Thread.sleep(2000);
            System.out.println("Disconnecting...");
            connection.disconnect();
            amIConnected(connection);
        } catch (SmackException | IOException | XMPPException | InterruptedException ex) {
            Logger.getLogger(TesMain.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void amIConnected(XMPPConnection con) {
        System.out.print("Am I connected? ");
        if (con.isConnected()) {
            System.out.println("You are connected.");
        } else {
            System.out.println("No, you are not.");
        }
    }
}
